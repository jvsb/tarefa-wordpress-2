<?php
    // Template Name: Home Page
?>

<?php get_header(); ?>
<section class="container">
    <div class="imgcafe" >
    <h1 class="h1"><?php the_field('titulo_chamativo') ?></h1>
    <a class="traco">_</a>
    <a class="text1"><?php the_field('sub-titulo_chamativo') ?></a>
    </div>
    <div class="posicao2" id="ancora1">
    <h2 class="h2"><?php the_field('texto1') ?></h2>
    <tr>
    <td>
    <div class="div2">
        <img src="<?php the_field('imagem1') ?>" alt="xicara de café">
        <h3 class="h3-1"><?php the_field('descricao1') ?></h3>        
    </div>
    </td>
    <td>
    <div class="div2">
        <img class="img2" src="<?php the_field('imagem2') ?>" alt="xicara de café">
        <h3 class="h3-2"><?php the_field('descricao2') ?></h3>
    </div>
    </td>
    </tr>
    <p class="psec2"><?php the_field('texto2') ?></p>
    </div>
    </section>
    <div class="section3" id="ancora2">
            <div class="estados">
            <div class="bola1"></div>
            <h4 id="h4"><?php the_field('bolatitulo1') ?></h4>
            <p class="p3"><?php the_field('bolatexto1') ?></p>
            </div>
            <div class="estados">
            <div class="bola2"></div>
            <h4 id="h4"><?php the_field('bolatitulo2') ?></h4>
            <p class="p3"><?php the_field('bolatexto2') ?></p>
            </div>
            <div class="estados">
            <div class="bola3"></div>
            <h4 id="h4"><?php the_field('bolatitulo3') ?></h4>
            <p class="p3"><?php the_field('bolatexto3') ?></p>
            <button class="saiba">SAIBA MAIS</button>
            </div>
    </div>
    <section class="locais">
    <div class="divs1">
    <div class="img-local">
    <img src="<?php the_field('localimagem1') ?>" alt="loja botafogo brafé">
    </div>
    <div class="texto-local">
    <h5><?php the_field('localtitulo1') ?></h5>
    <p><?php the_field('localtexto1') ?></p>
            <button>VER MAPA</button>
</div>
    </div>
    <div class="divs2">
    <div class="img-local">
    <img src="<?php the_field('localimagem2') ?>" alt="loja iguatemi brafé">
    </div>
    <div class="texto-local">
    <h5><?php the_field('localtitulo2') ?></h5>
    <p><?php the_field('localtexto2') ?></p>
            <button>VER MAPA</button>
</div>
    </div>
    <div class="divs3">
    <div class="img-local">
        <img src="<?php the_field('localimagem3') ?>" alt="loja Mineirão brafé">
    </div>
    <div class="texto-local">
        <h5><?php the_field('localtitulo3') ?></h5>
        <p><?php the_field('localtexto3') ?></p>
            <button>VER MAPA</button>
    </div>
    </div>
    </section>
    <section class="section4" id="ancora3">
            <div class="assine">
                <div class="posicao3">
                <h6>Assine Nossa Newsletter</h6>
                <p>promoções e eventos mensais</p>
            </div>
                <form>
                    <label></label>
                    <input type="text" placeholder="Digite seu e-mail">
                    <button type="submit">Enviar</button>
                </form>
            </div>
    </section>
    <?php get_footer(); ?>